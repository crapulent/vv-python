import io
import json
import os
import subprocess
import sys
from pathlib import Path, PurePath
from typing import Annotated, Optional
from dotenv import dotenv_values
import yaml
import typer

from vv_python.util import b64, q

app = typer.Typer()
secret = typer.Typer()
app.add_typer(secret, name='secret')


def read_cluster_config(leaf):
    check_levels = 10
    current_dir = leaf
    current_level = check_levels
    cluster_config = {}
    while current_level >= 0:
        file_candidate = current_dir / "vv.env"
        env_candidate = current_dir / ".env"
        if env_candidate.exists():
            content = dotenv_values(str(env_candidate))
            for k, v in content.items():
                if k not in cluster_config:
                    cluster_config[k] = {"value": v, "src": str(env_candidate)}
        if file_candidate.exists():
            content = dotenv_values(str(file_candidate))
            for k, v in content.items():
                if k not in cluster_config:
                    cluster_config[k] = {"value": v, "src": str(file_candidate)}
            if content.get('CLUSTER_ROOT', False):
                break
        current_dir = current_dir.parent
        current_level += 1
    return cluster_config


def k8s_secret_yaml(values: dict, name='my_secret', ns="default"):
    secret = dict(
        apiVersion='v1',
        kind='Secret',
        metadata=dict(
            name=name,
            namespace=ns,
        ),
        data={k: b64(v) for k, v in values.items()},
    )
    secret_yaml = yaml.dump(secret)
    return secret_yaml


@secret.command()
def update(file: str, src=None, git: Annotated[Optional[bool], typer.Option()] = None):
    config = read_cluster_config(Path(file).absolute().parent)
    if "SEALED_SECRETS_CERT" in config and config['SEALED_SECRETS_CERT']['value']:
        ssc = PurePath(Path(config['SEALED_SECRETS_CERT']['src']).parent,
                       config['SEALED_SECRETS_CERT']['value']).as_posix()
    else:
        raise NotImplemented("Need SEALED_SECRETS_CERT")
    if src is None:
        print("Please enter variables in .env format, finish with ctrl+D")
        sys.stderr.write(">")
        sys.stderr.flush()
        values = dotenv_values(stream=sys.stdin)
    else:
        values = dotenv_values(file)
    print("Found", values.keys())
    secret_yaml = k8s_secret_yaml(values)
    e = os.environ.copy()
    if "SEALED_SECRETS_CERT" not in e and ssc:
        e["SEALED_SECRETS_CERT"] = ssc
    full_cmd = ('kubeseal', '--merge-into', file, '-o', 'yaml')
    sealed_secret_cmd = subprocess.Popen(
        full_cmd,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        env=e,
    )
    sealed_secret, error = sealed_secret_cmd.communicate(secret_yaml.encode())
    if error != b"":
        print(full_cmd)
        print(error)
        print()
        raise Exception("kubeseal failed")
    print("Done")


@app.command()
def seal(file: str,
         docker: Annotated[bool, typer.Option()] = False,
         cluster_wide: Annotated[bool, typer.Option()] = False,
         namespace: Annotated[str, typer.Option()] = '',
         write: Annotated[bool, typer.Option()] = False):
    print(f"Going to seal '{file}'")
    contents = dotenv_values(file)
    to_seal = {k: v for k, v in contents.items() if not k.startswith('_')}
    config = {k: v for k, v in contents.items() if k.startswith('_')}

    ns = None
    if not namespace:
        kzn_root = config.get('_KUSTOMIZATION_ROOT', None)
        if kzn_root:
            root = (Path(file).absolute().parent / kzn_root).resolve()
            kzn_path = str(root / 'kustomization.yaml')
            with open(kzn_path, 'r') as stream:
                kzn = yaml.safe_load(stream)
            ns = kzn.get('namespace', None)
            print(f"Detected {ns=} for secret")
        else:
            file_dir = Path(file).absolute().parent
            kzn_nearest = file_dir / 'kustomization.yaml'
            if kzn_nearest.exists():
                with open(kzn_nearest, 'r') as stream:
                    kzn = yaml.safe_load(stream)
                ns = kzn.get('namespace', None)
                if ns:
                    print(f"Detected {ns=} for secret")
            if not ns:
                kzn_nearest = Path('kustomization.yaml')
                if kzn_nearest.exists():
                    with open(kzn_nearest, 'r') as stream:
                        kzn = yaml.safe_load(stream)
                    ns = kzn.get('namespace', None)
        if not ns:
            raise Exception("Need namespace")
    else:
        ns = namespace

    check_levels = 10
    current_dir = Path(file).absolute().parent
    current_level = check_levels
    cluster_config = {}
    while current_level >= 0:
        file_candidate = current_dir / "vv.env"
        env_candidate = current_dir / ".env"
        if env_candidate.exists():
            content = dotenv_values(str(env_candidate))
            cluster_config.update(**content)
        if file_candidate.exists():
            content = dotenv_values(str(file_candidate))
            cluster_config.update(**content)
            if content.get('CLUSTER_ROOT', False):
                break
        current_dir = current_dir.parent
        current_level += 1

    kubeconfig = os.getenv("KUBECONFIG", cluster_config.get('KUBECONFIG', None))

    secret_name = Path(file).name
    if secret_name.endswith(".secret"):
        secret_name = secret_name[:-7]
    if secret_name.endswith(".env"):
        secret_name = secret_name[:-4]
    annotations = {}
    if cluster_wide:
        annotations["reflector.v1.k8s.emberstack.com/reflection-allowed"] = "true"
        annotations["reflector.v1.k8s.emberstack.com/reflection-auto-enabled"] = "true"

    if docker:
        server = to_seal.get('SERVER')
        login = to_seal.get('LOGIN')
        password = to_seal.get('PASSWORD')
        auth_encoded = b64(f"{login}:{password}")
        auth = json.dumps({"auths":
                               {server:
                                    {"username": login,
                                     "password": password,
                                     "auth": auth_encoded
                                     }
                                }
                           }
                          )
        encoded_auth = b64(auth)
        data = {
            "data": {
                ".dockerconfigjson": encoded_auth
            },
            "type": "kubernetes.io/dockerconfigjson"
        }
    else:
        data = {"data": {k: b64(v) for k, v in to_seal.items()}}
    secret = dict(
        apiVersion='v1',
        kind='Secret',
        metadata=dict(
            name=secret_name,
            namespace=ns,
            annotations=annotations,
        ),
        **data,
    )
    secret_yaml = yaml.dump(secret)

    try:
        args = ['-o', 'yaml']
        cns = cluster_config.get('SEALED_SECRETS_NS', None)
        e = os.environ.copy()
        if cns:
            args.append('--controller-namespace')
            args.append(cns)
            print(f"Setting --controller-namespace for kubeseal as {q(cns)}")
            if kubeconfig:
                e['KUBECONFIG'] = str(Path(os.path.expanduser(kubeconfig)).resolve().absolute())
                print(f"Settings KUBECONFIG for kubeseal as {q(e['KUBECONFIG'])}")

        if cluster_config.get("SEALED_SECRETS_CERT") and "SEALED_SECRETS_CERT" not in e:
            ssc = PurePath(Path(current_dir),
                           cluster_config.get("SEALED_SECRETS_CERT")).as_posix()
            print(f"Setting SEALED_SECRETS_CERT for kubeseal as {q(ssc)}")
            e["SEALED_SECRETS_CERT"] = ssc

        full_cmd = ('kubeseal', *args)
        sealed_secret_cmd = subprocess.Popen(
            full_cmd,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            env=e,
        )
        # TODO: FileNotFoundError: [Errno 2] No such file or directory: 'kubeseal' => https://github.com/bitnami-labs/sealed-secrets?tab=readme-ov-file#kubeseal
        sealed_secret, error = sealed_secret_cmd.communicate(secret_yaml.encode())
        if error != b"":
            print(full_cmd)
            print(error)
            print()
            raise Exception("kubeseal failed")
        out_path = str(Path(file).parent / f"{secret_name}-sealed.yaml")
        with open(out_path, "wb") as sealed_file:
            sealed_file.write(sealed_secret)
        print(f"Secret has been sealed and saved as {q(out_path)}")
    except subprocess.CalledProcessError as e:
        print(f"An error occurred: {e}")
        raise Exception("kubeseal failed")

    kzn = Path(file).parent / 'kustomization.yaml'
    if kzn.exists():
        from ruamel.yaml import YAML
        # Create an instance of YAML
        yaml_reader = YAML()
        yaml_reader.preserve_quotes = True  # Preserve quotes in the YAML file
        yaml_reader.indent(mapping=2, sequence=4, offset=2)  # Set indentation configuration if needed

        with open(str(kzn), 'r') as file:
            kzn_content = yaml_reader.load(file)
        resources = kzn_content.get("resources", [])
        out_name = Path(out_path).name
        if out_name not in resources:
            resources.append(out_name)
            output = io.StringIO()
            yaml_reader.dump(kzn_content, output)
            if not write:
                print(output.getvalue())
            else:
                with open(str(kzn), 'w') as file:
                    file.write(output.getvalue())
                    print("Kustomization updated")


@app.callback()
def callback():
    pass


if __name__ == "__main__":
    print(sys.argv)
    app()
