import base64
import json


def b64(s: str) -> str:
    return base64.b64encode(s.encode("utf-8")).decode("utf8")


def q(s: str) -> str:
    return json.dumps(s)
