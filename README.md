FluxCD + kubeseal helper tooling

# Seal

1. I place `vv.env` file in fluxcd cluster root with following content:
    ```dotenv
    SEALED_SECRETS_NS=infra-sealed-secrets
    CLUSTER_ROOT=True
    ```
   and keep this file in source control

   Also I want to support `SEALED_SECRETS_PUBLIC_KEY=` var

2. I place `.env` file in fluxcd cluster root with following content:

   ```
   KUBECONFIG=~/.kube/xxx.yaml
   ```

   And I keep this wile out of source control (because it is user dependent)

3. I create something.env.secret if flux repo and fill all secrets there. I keep all *.secret files ignored by source
   control

4. I can add

   ```dotenv
   _KUSTOMIZATION_ROOT=../
   ```
   as I store namespace in kustomization.yaml on upper level. It is also possible to define namespace with --namespace
   cmd arg

5. Seal:

   ```bash
   vv seal something.env.secret
   ```

   This will:
    - deduce secret namespace
    - generate secret yaml
    - forward it to kubeseal with `--controller-namespace` arg set up
    - place output next to the env with naming {name}.env.secret => {name}-sealed.yaml
    - if kustomization is found on the same level as env it will add resulting yaml file to resources array (if --write
      is provided)
    - now it is up to you to check everything and make commit but i am considering auto-generating commit