import argparse
import json
import subprocess
import base64
from dotenv import dotenv_values
import yaml


# Encode the Docker credentials
def b64(s: str) -> str:
    return base64.b64encode(s.encode("utf-8")).decode("utf8")


def seal_docker_creds(secret_name, env_vars, args):
    server = env_vars.get('SERVER')
    login = env_vars.get('LOGIN')
    password = env_vars.get('PASSWORD')

    auth_encoded = b64(f"{login}:{password}")

    # Define the Kubernetes secret manifest with the labels for Kube Reflector
    auth = json.dumps({"auths": {server: {"username": login, "password": password, "auth": auth_encoded}}})
    encoded_auth = b64(auth)
    annotations = {
        "reflector.v1.k8s.emberstack.com/reflection-allowed": "true",
        "reflector.v1.k8s.emberstack.com/reflection-auto-enabled": "true", } if getattr(args, 'global', None) else {}
    secret_manifest_data = {
        "apiVersion": "v1",
        "kind": "Secret",
        "metadata": {
            "name": secret_name,
            "namespace": args.namespace,
            "annotations": annotations
        },
        "data": {
            ".dockerconfigjson": encoded_auth
        },
        "type": "kubernetes.io/dockerconfigjson"
    }
    secret_manifest = json.dumps(secret_manifest_data, indent=2)

    with open(f"{secret_name}-secret.yaml", "w") as secret_file:
        secret_file.write(secret_manifest)


def main():
    parser = argparse.ArgumentParser(description='Seal a Kubernetes Secret with Sealed Secrets.')
    parser.add_argument('seal', action='store_const', const=True, help='Enable sealing of the secret')
    parser.add_argument('env_path', type=str, help='Path to the .env file containing credentials')
    parser.add_argument('--global', action='store_true', help='Copy secret globally')
    parser.add_argument('--ns', dest='namespace', type=str, help='Kubernetes namespace for the sealed secret')
    parser.add_argument(
        '--sealns',
        dest='seal_namespace',
        type=str,
        help='Kubernetes namespace where sealed-secrets-controller lives'
    )
    args = parser.parse_args()

    secret_name = args.env_path.split('/')[-1].split('.')[0]

    if not args.namespace:
        # search for kustomization.yaml in the directory near the secret env file
        env_dir = '/'.join(args.env_path.split('/')[:-1])
        kustomization_path = f"{env_dir}/kustomization.yaml"
        try:
            kustomization = yaml.safe_load(open(kustomization_path))
            args.namespace = kustomization['namespace']
        except FileNotFoundError:
            print(f"Could not find kustomization.yaml in {env_dir}")
            exit(1)

    env_vars = dotenv_values(args.env_path)
    if 'SERVER' in env_vars and 'LOGIN' in env_vars and 'PASSWORD' in env_vars:
        seal_docker_creds(secret_name, env_vars, args)
    else:
        cmd = (f"kubectl create secret generic {secret_name} --from-env-file={args.env_path} --dry-run=client -o yaml "
               f" --namespace {args.namespace} "
               f"> {secret_name}-secret.yaml")
        print(f"Creating secret manifest with command: {cmd}")
        subprocess.run(
            cmd,
            shell=True
        )

    try:
        sealed_secret_cmd = subprocess.Popen(
            ('kubeseal', '--controller-namespace', args.seal_namespace, '-o', 'yaml'),
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE
        )

        secret_manifest = open(f"{secret_name}-secret.yaml").read()

        sealed_secret, error = sealed_secret_cmd.communicate(secret_manifest.encode())
        with open(f"{secret_name}-sealed.yaml", "wb") as sealed_file:
            sealed_file.write(sealed_secret)

        print(f"Secret has been sealed and saved as {secret_name}-sealed.yaml")
    except subprocess.CalledProcessError as e:
        print(f"An error occurred: {e}")


if __name__ == "__main__":
    main()
